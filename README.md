## About the project

Powering wireless sensors and IoT devices installed in remote hard-to-reach locations, such an hydroelectric power plants, can be complicated. Technologies such as energy harvesting allows systems and devices to be powered autonomously using energy sources available from the surrounding environment.

---

## About MicroPython

MicroPython is a lean and efficient implementation of the Python 3 programming language that includes a small subset of the Python standard library and is optimised to run on microcontrollers and in constrained environments.
MicroPython is packed full of advanced features such as an interactive prompt, arbitrary precision integers, closures, list comprehension, generators, exception handling and more. Yet it is compact enough to fit and run within just 256k of code space and 16k of RAM.
MicroPython aims to be as compatible with normal Python as possible to allow you to transfer code with ease from the desktop to a microcontroller or embedded system.
Using MicroPython allowed a faster code development and it increased the readability of the code.

---

## About Pycom & FiPy

Pycom is an ecosystems that makes IoT development easy, with the goal to help developers to have a super-fast journey from rapid prototyping to enterprise-grade deployment. Pycom's boards and mdoules consume a fraction of the energy of tradional modules and they enable multi-network applications. Besides that, Micropython community is quickly growing.
FiPy is one perfectly-formed with small-foot-print, IoT development board, made by Pycom. FiPy supports MicroPython, and features WiFi, Bluetooth, LoRa, Sigfox and dual LTE-M (CAT M1 and NBIoT). In this way you can access to all the world’s LPWAN networks on one tiny board.

---

## Getting started

For getting started:

1. Clone this repository in your local environment.
2. Set up your local environment, for starting coding.
3. Set up your FiPy board.
4. See documentation for more information.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).

---

## Local environment setup

To start working with your FiPy board you need to set up a suite of Pycom tools. Use these steps to install drivers and set up your development environment.

1. Install [Pycom drivers](https://docs.pycom.io/gettingstarted/installation/drivers/)
2. Set up your development environment with [Pymakr plugin](https://docs.pycom.io/gettingstarted/installation/pymakr/)

---

## How to set up your FiPy board

First you need to flash your FiPy board with MicroPython. To do that use Pycom Firmware Update tool. If you don't yet have it, [donwload and install it](https://docs.pycom.io/gettingstarted/installation/firmwaretool/).
Using Pycom Firmware Update tool you can flash three different version type of MicroPython:
- Pybytes
- Stable
- Development
To respect the requirements of this project you need to flash **stable** version type.
It is also highly recommended to *erase flash file system* when you upgrade firmware version.

At this poing you should be able to flash the project in this repository to your FiPy board using Pymakr plugin.

---

## Other documentation

You can find the complete documentation about this project in the folder *docs*.

---

## Other

If you use *Visual Studio Code* as editor for your project, you could see some *pylint(import-error)*. This is because pylint will try to import some built-in MicroPython modules not installed in your computer. You can avoid this disabling Python linting in VSCode.