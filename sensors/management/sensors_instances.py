from features.utility import log
from sensors.management.sensors_initializer import SensorsInitializer
from machine import I2C


class SensorsInstances:
    """
    It contains the instance of sensors. In this way they can be easily shared

    """

    # sensors you can initialize
    __BMP180 = "bmp180"
    __MQ2 = "mq2"
    __NOTHING = "nothing" # this is not a real sensor, but it was useful to test some code

    def __init__(self, sensors_info):
        self.__sensors_info = sensors_info
        self.__sensors_to_initialize = self.__sensors_info['sensors_to_initialize']
        self.__sensors_initializer = SensorsInitializer()

        self.__i2c = I2C()

        # SENSORS INSTANCES
        self.__ads1115 = self.__sensors_initializer.ads1115_init(self.__i2c)
        self.__nothing = None
        self.__bmp180 = None
        self.__mq2 = None


    def create_instances(self):
        """
        Method to initialize sensors.
        It will be only initialized sensors in the list you can find in __sensors_info['sensors_to_initialize']

        """
        # nothing initialitation
        if (self.__NOTHING in self.__sensors_to_initialize):
            self.__nothing = self.__sensors_initializer.nothing_init()
        else:
            self.__nothing = None
        # bmp180 initialitation
        if (self.__BMP180 in self.__sensors_to_initialize):
            self.__bmp180 = self.__sensors_initializer.bmp180_init(self.__i2c)
        else:
            self.__bmp180 = None
        # mq2 initialitation
        if (self.__MQ2 in self.__sensors_to_initialize):
            self.__mq2 = self.__sensors_initializer.mq2_init(self.__ads1115)
        else:
            self.__mq2 = None
        # WORK: implement other sensors #


    def get_nothing(self):
        return self.__nothing

    def get_bmp180(self):
        return self.__bmp180

    def get_mq2(self):
        return self.__mq2

    def get_ads1115(self):
        return self.__ads1115
