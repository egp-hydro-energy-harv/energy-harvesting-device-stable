from features.utility import log
from sensors.components.bmp085 import BMP180
from sensors.components.ads1x15 import ADS1115
from sensors.components.mq2 import MQ2


class SensorsInitializer:
    """
    Class to initialize sensors.
    Write here public method to initialize sensors.

    """

    # Pins
    P0 = 0
    P1 = 1
    P2 = 2
    P3 = 3

    def nothing_init(self):
        """
        To init nothing, that is not a real sensor, but it was useful to test some code
        You can take cue the implementation of Nothing to implement others sensors

        """
        log.v("SensorsInitializer", "Initialitation of Nothing sensor...")
        # write here code to init sensor
        nothing = 1
        log.v("SensorsInitializer", "Nothing sensor initialized")
        return nothing


    def bmp180_init(self, i2c, oversample=2, sealevel=100995):
        """
        Initializes the BMP180 sensor

        :param i2c: class <I2C> - i2c interface
        :param oversample: int - for setting the oversample parameter of BMP180 sensor. The defaul is 2.
                                 For more details, click here: https://github.com/robert-hh/BMP085_BMP180
        :param sealevel: int - you can set pressure for altitude calculation. The default is 1009,95 hPa,
                               but you can use your local QNH in hPa, or set a local pressure to determine
                               altitude difference
        :return bmp: BMP180
        """
        log.v("SensorInitializer", "Initialization of BMP180 sensor...")

        try:
            bmp = BMP180(i2c)
            bmp.oversample = oversample
            bmp.sealevel = sealevel
            log.v("SensorInitializer", "BMP180 sensor initialized")
            return bmp
        except Exception as ex:
            if (str(ex) == "I2C bus error"):
                log.e("SensorsInitializer", "Can't initialize BMP180 sensor for an I2C error, check if I2C wires are correctly connected")
            else:
                log.e("SensorsInitializer", "{}".format(ex))
            return None


    def ads1115_init(self, i2c, address=0x48, gain=1):
        """
        Initializes the ADS1115, implemented in this project through the library at following link:
        https://github.com/robert-hh/ads1x15
        More about ADS1115 addressing and gain here:
        http://henrysbench.capnfatz.com/henrys-bench/arduino-voltage-measurements/arduino-ads1115-module-getting-started-tutorial/

        :param i2c: class <I2C> - i2c interface
        :param address: int - hexadecimal number representing the default address for ADS1115, that is usually 0x48.
        :param gain: int - it's specifies the gain for ADC conversion.
        :return ads1115: ADS1115
        """
        log.v("SensorInitializer", "Initialization ADS1115")

        # here can be written any code to init the ADS1115 #
        try:
            ads1115 = ADS1115(i2c, address, gain)
            return ads1115
        except Exception as ex:
            if (str(ex) == "I2C bus error"):
                log.e("SensorsInitializer", "Can't initialize ADS1115 for an I2C error, check if I2C wires are correctly connected")
            else:
                log.e("SensorsInitializer", "{}".format(ex))
            return None


    def mq2_init(self, ads1115, pin=P0, ro=None):
        """
        Initializes MQ2 gas sensor

        :param ads1115: class <ADS1115> - adc used to read mq2
        :param pin: int - pin of ADC convertor to use. Must be one of supported pins. Default is P0
        :param ro: int - Ro value of the sensor. Must be valid ro value. Defualt is to calibrate it
        """
        log.v("SensorInitializer", "Initialization MQ2 gas sensor")

        try:
            mq2 = MQ2(ads1115, pin, ro)
            return mq2
        except Exception as ex:
            if (str(ex) == "I2C bus error"):
                log.e("SensorsInitializer", "Can't initialize MQ2 sensor for an I2C error, check if I2C wires are correctly connected")
            else:
                log.e("SensorsInitializer", "{}".format(ex))
            return None
