from features.utility import log
from sensors.management.sensors_instances import SensorsInstances
from sensors.management.sensors_reader import SensorsReader


class SensorsManager:
    """
    SensorsManager has the scope to manage sensors.

    """

    def __init__(self, sensors_info):
        self.__sensors_info = sensors_info
        # contains instances of sensors
        self.__sensors_instances = SensorsInstances(self.__sensors_info)
        # for reading sensors
        self.sensors_reader = SensorsReader(self.__sensors_info, self.__sensors_instances)

    def create_instances(self):
        """
        Method to initialize sensors.

        """
        self.__sensors_instances.create_instances()
