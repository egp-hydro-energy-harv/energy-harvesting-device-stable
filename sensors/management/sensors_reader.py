from features.utility import log


class SensorsReader:
    """
    Class to read sensors

    """

    def __init__(self, sensors_info, sensors_instances):
        self.__sensors_info = sensors_info
        self.__sensors_instances = sensors_instances


    def read_nothing(self):
        if (self.__sensors_instances.get_nothing()):
            log.v("SensorsReader", "It would be funny to really read a Nothing sensor")
            import machine
            read_value = machine.rng()
            log.v("SensorsReader", "Nothing sensors value: " + str(read_value))
            return read_value
        else:
            log.e("SensorsReader", "Invalid attempt to read Nothing sensor, that is not initialized")
            return False


    def read_temperature(self):
        bmp180 = self.__sensors_instances.get_bmp180()

        if (bmp180):
            log.v("SensorsReader", "Reading temperature from BMP180 sensor...")
            temperature = bmp180.temperature
            log.v("SensorsReader", "Temperature: " + str(temperature))
            return temperature
        else:
            log.e("SensorsReader", "Invalid attempt to read BMP180 sensor, that is not initialized")
            return False


    def read_pressure(self):
        bmp180 = self.__sensors_instances.get_bmp180()

        if (bmp180):
            log.v("SensorsReader", "Reading pressure from BMP180 sensor...")
            pressure = bmp180.pressure
            log.v("SensorsReader", "Pressure: " + str(pressure))
            return pressure
        else:
            log.e("SensorsReader", "Invalid attempt to read BMP180 sensor, that is not initialized")
            return False


    def read_altitude(self):
        bmp180 = self.__sensors_instances.get_bmp180()

        if (bmp180):
            log.v("SensorsReader", "Reading altitude from BMP180 sensor...")
            altitude = bmp180.altitude
            log.v("SensorsReader", "Altitude: " + str(altitude))
            return altitude
        else:
            log.e("SensorsReader", "Invalid attempt to read BMP180 sensor, that is not initialized")
            return False


    def read_mq2(self):
        mq2 = self.__sensors_instances.get_mq2()

        if (mq2):
            log.v("SensorsReader", "Reading data from MQ2 sensors...")
            mq2_readings_raw = mq2.percentage()

            mq2_readings = {}
            mq2_readings['co_gas'] = mq2_readings_raw[0]
            mq2_readings['h2_gas'] = mq2_readings_raw[1]
            mq2_readings['ch4_gas'] = mq2_readings_raw[2]
            mq2_readings['lpg_gas'] = mq2_readings_raw[3]
            mq2_readings['propane_gas'] = mq2_readings_raw[4]
            mq2_readings['alcohol_gas'] = mq2_readings_raw[5]
            mq2_readings['smoke_gas'] = mq2_readings_raw[6]

            return mq2_readings
        else:
            log.e("SensorsReader", "Invalid attempt to read MQ2 sensor, that is not initialized")
            return False


    def read_ads1115(self, pin, rate=4):
        """
        Use this method if you want to read the voltage to one of four pins of ADS1115.
        Available pin from ADS1115 are:
        A0 => 0
        A1 => 1
        A2 => 2
        A3 => 3

        """
        ads1115 = self.__sensors_instances.get_ads1115()

        if (ads1115):
            if (pin < 0 or pin > 4):
                log.e("SensorsReader", "Tried to read pin n.{} of ADS1115, that has only four pins".format(pin))
                return False

            raw_value = ads1115.read(rate, pin)
            value = ads1115.raw_to_v(raw_value)
            return value
        else:
            log.e("SensorsReader", "Invalid attempt to read something from ADS1115, that is not initialized")
            return False
