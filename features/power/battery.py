from features.utility import log


class Battery:

    # ADS1115 pin to which the battery is connected
    __PIN = 1


    def __init__(self, battery_info):
        # battery info you can find in device_config.json
        self.battery_info = battery_info


    def read_voltage(self):
        # WORK: This is a stub function, it does not return a true value of battery voltage
        from sensors.components.ads1x15 import ADS1115
        from machine import I2C

        ads1115 = ADS1115(I2C())
        raw_value = ads1115.read(0, self.__PIN)
        voltage = ads1115.raw_to_v(raw_value) # WORK: fix that, not always correct

        return 3.80


    def percentage(self):
        voltage = self.read_voltage()
        min_voltage = self.battery_info['min_voltage']
        max_voltage = self.battery_info['max_voltage']

        percentage = (voltage - min_voltage) / (max_voltage - min_voltage) * 100
        log.v("Battery", "Percentage of battery charge: " + str(percentage) + "%")
        return percentage
