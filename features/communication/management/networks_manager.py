from features.utility import log
from features.communication.mqtt_components.mqtt import MQTTClient
from network import WLAN
from network import LTE
from network import LoRa
from machine import WDT
from machine import Timer


class NetworksManager:
    """
    Use NetworksManager to access to network drivers and routing configuration
    More info about WLAN module here: https://docs.pycom.io/firmwareapi/pycom/network/wlan.html
    More info about LTE module here: https://docs.pycom.io/firmwareapi/pycom/network/lte.html
    More info about LoRa module here: https://docs.pycom.io/firmwareapi/pycom/network/lora.html

    More info about MQTT here: https://learn.adafruit.com/adafruit-io/mqtt-api
    """

    # networks technologies that can be enabled to send data to Internet
    WIFI = "wifi"
    LTE = "lte"
    LORA = "lora"

    # timeout for __wifi_lte_watchdog
    __TIMEOUT_MILLISECONDS = 8000

    __NETWORK_TYPE_WIFI = 0
    __NETWORK_TYPE_LORA = 1
    __NETWORK_TYPE_SIGFOX = 2
    __NETWORK_TYPE_LTE = 3

    __CONNECTION_STATUS_DISCONNECTED = 0
    __CONNECTION_STATUS_CONNECTED_MQTT_WIFI = 1
    __CONNECTION_STATUS_CONNECTED_MQTT_LTE = 2
    __CONNECTION_STATUS_CONNECTED_LORA = 3
    __CONNECTION_STATUS_CONNECTED_SIGFOX = 4


    def __init__(self, device_config):
        self.__device_config = device_config
        self.network_preferences = self.__device_config['network_preferences']

        # it indicates the type of network to which the device is connected
        self.__network_type = None
        # it indicates the connection (MQTT, etc...)
        self.__connection = None
        # it indicates if device is connected to Internet
        self.__connection_status = self.__CONNECTION_STATUS_DISCONNECTED
        # device id
        self.__device_id = self.__device_config['device_id']
        # username
        self.__user_name = self.__device_config['username']
        # password
        self.__password = self.__device_config['password']
        # host
        self.__host = self.__device_config['server']
        # port
        self.__port = self.__device_config['port']
        # mqtt topics
        self.__mqtt_download_topic = self.__user_name + "/f/#"
        self.__mqtt_upload_topic = self.__user_name + "/f/outdoor"
        # ssl and ssl parameters
        self.__ssl = self.__device_config.get('ssl', False)
        self.__ssl_params = self.__device_config.get('ssl_params', {})

        self.__wlan = None
        self.__lte = None
        self.__lora = None


    def connect_wifi(self):
        # WORK: maybe here you should initialise a watchdog

        log.v("NetworksManager", "Started to connect to WiFi")

        if (self.__connection_status != self.__CONNECTION_STATUS_DISCONNECTED):
            log.w("NetworksManager", "Error connect_wifi:  Connection already exists. Disconnect first")
            return False
        try:
            antenna = self.__device_config.get('wlan_antenna', WLAN.INT_ANT)
            known_nets = [((self.__device_config['wifi']['ssid'], self.__device_config['wifi']['password']))]
            if antenna == WLAN.EXT_ANT:
                log.w("NetworksManager", "Using external WiFi antenna")
            self.__wlan = WLAN(mode=WLAN.STA, antenna=antenna)

            available_nets = self.__wlan.scan()
            nets = frozenset([e.ssid for e in available_nets])
            known_nets_names = frozenset([e[0] for e in known_nets])
            net_to_use = list(nets & known_nets_names)
            try:
                net_to_use = net_to_use[0]
                pwd = dict(known_nets)[net_to_use]
                sec = [e.sec for e in available_nets if e.ssid == net_to_use][0]
                self.__wlan.connect(net_to_use, (sec, pwd), timeout=10000)
                while (not self.__wlan.isconnected()):
                    import time
                    time.sleep(0.1)
            except Exception as e:
                if (str(e) == "list index out of range"):
                    log.w("NetworksManager", "Please review WiFi SSID and password inside config")
                else:
                    log.e("NetworksManager", "Error connecting using WiFi: %s" % e)

                self.deinit_wifi()
                return False
            self.__network_type = self.__NETWORK_TYPE_WIFI
            log.v("NetworksManager", "WiFi connection successfully established")
            log.v("NetworksManager", "Details of WiFi connection: {}".format(self.__wlan.ifconfig()))

            return True
        except Exception as ex:
            log.e("NetworksManager", "Exception connect_wifi: {}".format(ex))
            return False


    def disconnect_wifi(self):
        # disconnect from the WiFi access point
        if (self.__wlan):
            if (self.__wlan.isconnected()):
                log.v("NetworksManager", "Disconnected from the WiFi access point")
                self.__wlan.disconnect()
        else:
            log.e("NetworksManager", "Invalid attempt to disconnect from WiFi, but WLAN object is not initialized")


    def deinit_wifi(self):
        if (self.__wlan):
            log.v("NetworksManager", "WiFi radio disabled")
            # disables the WiFi radio
            self.__wlan.deinit()
        else:
            log.e("NetworksManager", "Invalid attempt to disable WiFi radio, but WLAN object is not initialized")


    def disconnect_lte(self):
        if (self.__lte):
            # end the data session with the LTE network
            if (self.__lte.isconnected()):
                log.v("NetworksManager", "Disconnected from LTE network")
                self.__lte.disconnect()
        else:
            log.e("NetworksManager", "Invalid attempt to disconnect from LTE network, but LTE object is not initialized")


    def deinit_lte(self, detach=True, reset=False):
        if (self.__lte):
            # disables LTE modem completely. This reduces
            # the power consumption to the minimum. Call this before entering deepsleep
            if (self.__lte.isattached()):
                log.v("NetworksManager", "LTE modem disabled")
                self.__lte.deinit()
        else:
            log.e("NetworksManager", "Invalid attempt to disable LTE modem, but LTE object is not initialized")


    def sleep_lora(self):
        if (self.__lora):
            log.v("NetworksManager", "LoRa modem put in sleep mode")
            self.__lora.power_mode(LoRa.SLEEP)
        else:
            log.e("NetworksManager", "Invalid attempt to put LoRa mdoem in sleep mode, but LoRa object is not initialized")


    def disable_connections(self):
        """
        To disable open and active Internet connection (WiFi, LoRa, NB-IoT)

        """
        if (self.WIFI in self.network_preferences):
            self.disconnect_wifi()
            self.deinit_wifi()
        if (self.LTE in self.network_preferences):
            self.disconnect_lte()
            self.deinit_lte()
        if (self.LORA in self.network_preferences):
            self.sleep_lora()


    def start_mqtt(self, reconnect=True, check_interval=0.5):
        """
        Open a connection to MQTT broker

        """
        try:
            self.__connection = MQTTClient(self.__device_id, self.__host, self.__mqtt_download_topic,
                                           user=self.__user_name, password=self.__password, port=self.__port)
            self.__connection_status = self.__CONNECTION_STATUS_CONNECTED_MQTT_WIFI
            self.__connection.connect()
            log.v("NetworksManager", "Connected to MQTT {}".format(self.__host))
            return True
        except Exception as ex:
            if ('{}'.format(ex) == '4'):
                log.e("NetworksManager", "MQTT ERROR! Bad credentials when connecting to server: '{}'".format(self.__host))
            else:
                log.e("NetworksManager", "MQTT ERROR! {}".format(ex))
            return False


    def stop_mqtt(self):
        """
        Disconnect from MQTT broker

        """
        self.__connection.disconnect()
        self.__connection_status = self.__CONNECTION_STATUS_DISCONNECTED
        log.v("NetworksManager", "Now disconnected from MQTT broker {}".format(self.__host))


    def send_sensor_readings(self, sensors_readings):
        """
        To send sensors readings to MQTT broker

        :param sensors_readings: class <dict> - a dictionary representing sensors readings in a JSON similiar format
        """
        log.v("NetworksManager", "Sending sensor readings to {}".format(self.__host))
        log.v("NetworksManager", "MQTT upload topic is '{}'".format(self.__mqtt_upload_topic))
        self.__connection.publish(self.__mqtt_upload_topic, str(sensors_readings))
