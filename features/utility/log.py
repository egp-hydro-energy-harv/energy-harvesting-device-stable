import os


__PATH = '/flash/'  # str - indicates a file path where log have to be saved
__DEBUG = True  # debug: boolean - to abilitate debugging


def v(tag, msg):
    """
    VERBOSE log message

    :param tag: str - used to identify the source of a log message. It usually
                identifies the class or activity where the log call occurs. This
                value may be None
    :param msg: str - the message you would like logged. This value must never be None
    """
    if (__DEBUG):
        log = tag + " | " + msg
        print(log)
        __write(log)


def w(tag, msg):
    """
    WARN log message

    :param tag: str - used to identify the source of a log message. It usually
                identifies the class or activity where the log call occurs. This
                value may be None
    :param msg: str - the message you would like logged. This value must never be None
    """
    if (__DEBUG):
        log = "WARNING | " + tag + " | " + msg
        print(log)
        __write(log)


def e(tag, msg):
    """
    ERROR log message

    :param tag: str - used to identify the source of a log message. It usually
                identifies the class or activity where the log call occurs. This
                value may be None
    :param msg: str - the message you would like logged. This value must never be None
    """
    if (__DEBUG):
        log = "ERROR | " + tag + " | " + msg
        print(log)
        __write(log)


def rawprint(s, end=''):
    """
    Simply print something on console

    :param s: str - the message you would like to be printed on console
    """
    if (__DEBUG):
        print(s, end=end)


def __write(log):
    """
    Write the log message in a logger file specified by path attribute

    :param log: str - log message that have to be saved in a logger file
    """
    if (__PATH is not None):
        f = open(__PATH + 'log.txt', 'a')
        f.write(log + "\n")
        f.close()
