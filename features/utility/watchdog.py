from features.utility import log
from machine import WDT


class Watchdog:
    """
    The watchdog is used to restart the system when the application crashes and ends up
    into a non recoverable state. After enabling, the application must 'feed' the watchdog
    periodically to prevent it from expiring and resetting the system

    """

    __WDT_TIMEOUT = 50000


    def __init__(self, enable_watchdog):
        """

        :param enable_watchdog: class <bool> - it indicated if watchdog have to be enabled
        """
        self.__enable_watchdog = enable_watchdog
        self.__wdt = None

    def start(self):
        if (self.__enable_watchdog):
            self.__wdt = WDT(timeout=self.__WDT_TIMEOUT)
            log.v("Watchdog", "Watchdog enabled")

    def feed(self):
        if (self.__enable_watchdog and self.__wdt):
            self.__wdt.feed()
            log.v("Watchdog", "Watchdog feeded")
