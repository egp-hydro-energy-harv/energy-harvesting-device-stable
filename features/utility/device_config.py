from features.utility import log


class DeviceConfig:
    """
    Reads from device_config.json file the configuration of the device_id

    """

    def read_config(self, file='/flash/device_config.json'):
        device_config = {}
        # WORK TO DO: Implement code for using force_update in this method
        # You can take cue seeing at the implementation of Pybytes Library
        force_update = False

        try:
            import json
            f = open(file, 'r')
            jfile = f.read()
            f.close()
            try:
                device_config = json.loads(jfile.strip())
                device_config['cfg_msg'] = "Device configuration read from {}".format(file)
                log.v("DeviceConfig", device_config['cfg_msg'])
            except Exception as ex:
                log.v("DeviceConfig", "Error reading {} file\n Exception: {}".format(file, ex))
        except Exception as ex:
            force_update = True

        return device_config
