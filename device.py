from features.utility import log
from features.communication.management.networks_manager import NetworksManager
from features.utility.device_config import DeviceConfig
from features.utility.watchdog import Watchdog
from features.power.battery import Battery
from sensors.management.sensors_manager import SensorsManager


class Device:
    """
    Device is a class that represent the Energy-Harvesting-Device.
    This class have an instance called 'device' that is shared between
    every module, package, class, function, method of this project, more info here:
    https://stackoverflow.com/questions/16587244/python-sharing-class-variables-between-multiple-classes

    """

    # CONSTANTS
    BATTERY_PERCENTAGE_TORUN = 20


    def __init__(self):
        # it contains device configuration, read from device_config.json
        self.device_config = DeviceConfig().read_config()
        # to manage sensors (initialitation and reading)
        self.__sensors_manager = SensorsManager(self.device_config['sensors_info'])
        # for monitoring battery
        self.battery = Battery(self.device_config['battery_info'])
        # check if device is runnable, if not the device goes directly to sleep mode
        self.__is_runnable()

        # to manage networks and connections
        self.__networks_manager = NetworksManager(self.device_config)
        # watchdog
        self.__watchdog = Watchdog(self.device_config.get('enable_watchdog', True))


    def run(self):
        """
        To start running the device and execute its operations

        """
        log.v("Device", "Running device...")

        self.__set_booting_option()

        # start watchdog
        self.__watchdog.start()

        # initialitation of sensors
        self.__sensors_manager.create_instances()

        # feed watchdog
        self.__watchdog.feed()

        # readings sensors
        nothing_value = self.__sensors_manager.sensors_reader.read_nothing()
        temperature = self.__sensors_manager.sensors_reader.read_temperature()
        pressure = self.__sensors_manager.sensors_reader.read_pressure()
        altitude = self.__sensors_manager.sensors_reader.read_altitude()
        mq2_readings = self.__sensors_manager.sensors_reader.read_mq2()

        # sensors readings parsing
        sensors_readings = {}
        sensors_readings['nothing_value'] = nothing_value
        sensors_readings['temperature'] = temperature
        sensors_readings['pressure'] = pressure
        sensors_readings['altitude'] = altitude
        sensors_readings['gas'] = mq2_readings
        sensors_readings['battery_level'] = self.battery.percentage()

        # feed watchdog
        self.__watchdog.feed()

        # connecting to Internet
        self.__networks_manager.connect_wifi()

        # feed watchdog
        self.__watchdog.feed()

        # start MQTT session
        if (self.__networks_manager.start_mqtt()):
            # send sensor readings
            self.__networks_manager.send_sensor_readings(sensors_readings)
            # stop MQTT session
            self.__networks_manager.stop_mqtt()

        # feed watchdog
        self.__watchdog.feed()

        log.v("Device", "Running device successfully completed")


    def stop(self):
        """
        To stop the device from executing main operations

        """
        log.v("Device", "Stopping device from executing operations...")
        # disable active Internet connections (WiFi, LoRa or NB-IoT)
        self.__networks_manager.disable_connections()
        # turn off device
        self.__turn_off()
        log.v("Device", "Device stopped successfully")


    def __turn_off(self):
        """
        To turn off device

        """
        import machine

        log.v("Device", "Turning off device, to infinity... and beyond!")
        machine.deepsleep(self.device_config['sensors_info']['sending_interval'] * 60000)
        ###machine.deepsleep(900000) # for testing scopes


    def __is_runnable(self):
        """
        Checks percentage of battery ch-arge to understand if device has enough
        energy to complete initialitation and run.

        """
        percentage = self.battery.percentage()

        if (percentage < self.BATTERY_PERCENTAGE_TORUN):
            log.v("Device", "Not enough battery charge to run device")
            self.__turn_off()
        else:
            log.v("Device", "Percentage of battery charge at booting: " + str(percentage) + "%")


    def __set_booting_option(self):
        """
        To set up booting option, for next bootings of device

        """
        import pycom

        if (pycom.wifi_on_boot()):
            log.v("Device", "WiFi on boot is enable, now it will be disabled")
            pycom.wifi_on_boot(False)
        if (pycom.lte_modem_en_on_boot()):
            log.v("Device", "LTE modem on boot is enable, now it will disabled")
            pycom.lte_modem_en_on_boot(False)


device = Device()
