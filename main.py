from features.utility import log
from device import device


if __name__ == "__main__":
    device.run()
    device.stop()
